import Vue from 'vue'
import VueRouter from 'vue-router'
import index from "../pages/index"
import support from "../pages/support"
import test from "../pages/test"
Vue.use(VueRouter)
const routes = [
  {
    path:"/",
    name:"index",
    component:index
  },
  {
    path:"/support",
    name:"support",
    component:support 
  },
  {
    path:"/test",
    name:"test",
    component:test
  }
]
const router = new VueRouter({
  mode:"hash",
  routes
})
export default router;